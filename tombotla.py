#!/usr/bin/python3
# coding=utf-8
# tombotla.py
# a simple tombola Discord bot
import os
from dotenv import load_dotenv
import discord
from discord.ext import commands
from urllib.request import urlretrieve

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

def get_prefix(bot, message):
    prefixes = [
        'tombotla!',
        'tb!',
        'tomb!',
        'tombola!',
        'Tb!',
        'TB!',
        'TOMB!',
        'Tomb!',
        'TOMBOLA!',
        'Tombola!',
        'TOMBOTLA!',
        'Tombotla!'
    ]
    return prefixes

bot = commands.Bot(command_prefix=get_prefix,description='tombotla: A simple tombola management bot',case_insensitive=True)

cogs = ['cogs.Management']

@bot.listen()
async def on_message(ctx):
    if (ctx.author == bot.user):
        return
    if not (bot.user in ctx.mentions):
        return
    prefixes = tuple(get_prefix(bot,ctx))
    if (ctx.content.startswith(prefixes)):
        return
    await ctx.channel.send(f'Hey folks, did someone just say my name? Maybe {ctx.author.mention}?\nLearn how to use me by typing `tombotla!help`, or shorter: `tb!help`.')

@bot.event
async def on_ready():
    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name='@tombotla'))
    urlretrieve('https://openclipart.org/image/400px/svg_to_png/297835/publicdomainq-robot.png','avatar_img.png')
    filehandle = open('avatar_img.png', 'rb')
    imgpath = filehandle.read()
    await  bot.user.edit(avatar=imgpath)
    print(f'{bot.user.name} successfully connected to Discord, ready for managing tombolas!')
    for cog in cogs:
        bot.load_extension(cog)
    return

bot.run(TOKEN, bot=True, reconnect=True)