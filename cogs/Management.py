#!/usr/bin/python3
# coding=utf-8
# tombotla.py
# a simple tombola Discord bot -- command cogs
import random
import asyncio
import discord
from discord.ext import commands
from discord.ext.commands import bot_has_permissions

class Management(commands.Cog):
    
    def __init__(self, bot):
        self.bot = bot
        self.participants = []
        self.channels = []
        self.lockusers = []

    # announce
    @commands.command(
        name='announce',
        brief='create a tombola',
        help='This will create and announce a tombola.',
        aliases=['declare','start','setup','create','begin','beginn'],
        usage='[tombola announcement text]'
    )
    async def announce(self, ctx):
        if (ctx.message.channel.id in self.channels):
            lockuser = self.lockusers[self.channels.index(ctx.message.channel.id)]
            await ctx.send(f'Unable to create tombola; there\'s already a tombola running in this channel, announced by {lockuser.display_name}!')
            return
        text = ctx.message.content[len(ctx.prefix)+len(ctx.invoked_with)+1:] 
        if not isinstance(ctx.channel, discord.DMChannel):
            await ctx.message.delete()
            print(f'deleted original message by {ctx.author}: {ctx.message}')
        self.channels.append(ctx.message.channel.id)
        self.lockusers.append(ctx.author)
        self.participants.append([])
        await ctx.send(f'It\'s tombola time! {ctx.author.display_name} created a tombola.')
        if (len(text)>0) :
            await ctx.send(f'{text}')

    # raffle
    @commands.command(
        name='raffle',
        brief='raffle a tombola',
        help='This will raffle an announced tombola, and notify the tombola announcer as well as the winner via DM.',
        aliases=['roll','tombola','los']
    )
    async def raffle(self, ctx):
        if not (ctx.message.channel.id in self.channels):
            await ctx.send('Nothing to raffle, no tombola running :exploding_head:')
            return
        if not isinstance(ctx.channel, discord.DMChannel):
            await ctx.message.delete()
            print(f'deleted original message by {ctx.author}: {ctx.message}')
        tombola_index=self.channels.index(ctx.message.channel.id)
        lockuser = self.lockusers[tombola_index]
        participants = self.participants[tombola_index]
        if (len(participants)<1):
            await ctx.send('Tombola participant list is empty, nothing to raffle :exploding_head:')
            return
        self.lockusers.pop(tombola_index)
        self.participants.pop(tombola_index)
        self.channels.pop(tombola_index)
        participant_string = ', '.join([(username.mention) for username in participants])
        await ctx.send(f'Raffling {lockuser.display_name}\'s tombola for {participant_string} now… :slot_machine:')
        winner=random.choice(participants)
        await ctx.send('Good news, a winner has been drawn.')
        await ctx.send('And the winner is…')
        await ctx.send('…10…', delete_after=1.3)
        await asyncio.sleep(1.5)
        await ctx.send('…9…', delete_after=1.3)
        await asyncio.sleep(1.5)
        await ctx.send('…wait for it…', delete_after=1.3)
        await asyncio.sleep(1.5)
        await ctx.send('…7…', delete_after=1.3)
        await asyncio.sleep(1.5)
        await ctx.send('…dirtyhawk!', delete_after=1.3)
        await asyncio.sleep(1.5)
        await ctx.send('…just kidding…', delete_after=1.3)
        await asyncio.sleep(1.5)
        await ctx.send('…4…', delete_after=1.3)
        await asyncio.sleep(1.5)
        await ctx.send('…3…', delete_after=1.3)
        await asyncio.sleep(1.5)
        await ctx.send('…2…', delete_after=1.3)
        await asyncio.sleep(1.5)
        await ctx.send('…wait for it…', delete_after=1.3)
        await asyncio.sleep(1.5)
        await ctx.send(f'…{winner.mention} :gift:')
        if isinstance(ctx.channel, discord.DMChannel):
            await ctx.send(f'You won yourself.\nYou expected that, right?\nWho else should win in a direct message channel?\nWow, you really are desperate, aren\'t you?\nHere, take a gift to feel better: :gift::tada:')
            return
        await asyncio.sleep(2)
        await winner.send(f'Congratulations, {winner.display_name}! You won the tombola in {ctx.channel.mention} :tada:\nPlease contact {lockuser.display_name} to claim your price.')
        await lockuser.send(f'Hi {lockuser.display_name}! You raffled a tombola in {ctx.channel.mention}, and {winner.display_name} won :tada:\nPlease make contact to offer your price.')
    
    # abort
    @commands.command(
        name='abort',
        brief='abort a tombola',
        help='This will abort an announced tombola without warning.',
        aliases=['stop','abbruch'],
        hidden=True
    )
    async def abort(self, ctx):
        if not (ctx.message.channel.id in self.channels):
            await ctx.send(f'Nothing to abort, no tombola running!')
            return
        if not isinstance(ctx.channel, discord.DMChannel):
            await ctx.message.delete()
            print(f'deleted original message by {ctx.author}: {ctx.message}')
        tombola_index=self.channels.index(ctx.message.channel.id)
        lockuser = self.lockusers[tombola_index]
        self.lockusers.pop(tombola_index)
        self.participants.pop(tombola_index)
        self.channels.pop(tombola_index)
        await ctx.send(f'{lockuser.display_name}\'s tombola has been aborted by {ctx.author.display_name}.')

    # clear
    @commands.command(
        name='clear',
        brief='remove all participants from a tombola',
        help='This will remove *all* participants from a tombola without warning.',
        aliases=['purge','leeren'],
        hidden=True
    )
    async def clear(self, ctx):
        if not (ctx.message.channel.id in self.channels):
            await ctx.send(f'Nothing to clear, no tombola running!')
            return
        if not isinstance(ctx.channel, discord.DMChannel):
            await ctx.message.delete()
            print(f'deleted original message by {ctx.author}: {ctx.message}')
        tombola_index=self.channels.index(ctx.message.channel.id)
        self.participants[tombola_index] = []
        await ctx.send(f'cleared all participants from tombola')
    
    # shutdown
    @commands.command(
        name='shutdown',
        aliases=['verziehdich','hauab'],
        hidden=True
    )
    @commands.is_owner()
    async def shutdown(self,ctx):
        if not isinstance(ctx.channel, discord.DMChannel):
            await ctx.message.delete()
        await ctx.send('I\'m leaving for now; see you later, everyone.')
        print(f'received shutdown command by {ctx.author}, logging off')
        await ctx.bot.logout()

    # join
    @commands.command(
        name='join',
        brief='add participants to a tombola',
        help='This will enrol you or others for a tombola. Any person that is @mentioned will be added; if no @mention is found, only you will be added.',
        aliases=['add','me','enrol','ich','dabei','ichichich'],
        usage='[@mention] [@mention] […]'
    )
    async def join(self, ctx):
        mentions = ctx.message.mentions
        if (self.bot.user in mentions):
            mentions.remove(self.bot.user)
        if (len(mentions) > 0):
            add_participants = mentions
        else :
            add_participants = [ ctx.author ]
        if not (ctx.message.channel.id in self.channels):
            await ctx.send(f'There\'s currently no tombola running in this channel.')
            return
        tombola_index=self.channels.index(ctx.message.channel.id)
        for participant in add_participants:
            if not participant in self.participants[tombola_index]:
                self.participants[tombola_index].append(participant)
                print('list of participants is now: ', self.participants[tombola_index])
                await ctx.send(f'You are now registered for the tombola, {participant.mention}.')
            else:
                await ctx.send(f'Are you trying to register twice for a tombola, {participant.mention}? Please don\'t. It won\'t raise your chances.')
                print('did not add duplicate participant ', participant.name)

    # stepout
    @commands.command(
        name='stepout',
        brief='remove participants from a tombola',
        help='This will remove participants from a tombola. Any person that is @mentioned will be removed; if no @mention is found, only you will be removed.',
        aliases=['checkout','optout','remove','neelassmal','dochnicht','ichbinraus'],
        usage='[@mention] [@mention] […]'
    )
    async def stepout(self, ctx):
        mentions = ctx.message.mentions
        if (self.bot.user in mentions):
            mentions.remove(self.bot.user)
        if (len(mentions) > 0):
            remove_participants = mentions
        else :
            remove_participants = [ ctx.author ]
        if not (ctx.message.channel.id in self.channels):
            await ctx.send(f'There\'s currently no tombola running in this channel.')
            return
        tombola_index=self.channels.index(ctx.message.channel.id)
        for participant in remove_participants:
            if participant in self.participants[tombola_index]:
                self.participants[tombola_index].remove(participant)
                print('list of participants is now: ', self.participants)
                await ctx.send(f'Your registration for the tombola has been canceled, {participant.mention}!')
            else:
                await ctx.send(f'You never registered for the tombola, {participant.mention}!')
                print('did not remove unregistered participant ', participant.name)


    # query
    @commands.command(
        name='query',
        brief='infos about a tombola',
        help='This command will show information about an already announced tombola. It lists the tombola\'s announcer, and all registered participants.',
        aliases=['list','show','?']
    )
    async def query(self, ctx):
        if not (ctx.message.channel.id in self.channels):
            await ctx.send('Nothing to show, no tombola running :exploding_head:')
            return
        tombola_index=self.channels.index(ctx.message.channel.id)
        lockuser = self.lockusers[tombola_index]
        participants = self.participants[tombola_index]
        if (len(participants)<1):
            participant_string = '*None*'
        else:
            participant_string = ', '.join([(username.display_name) for username in participants])
        embed = discord.Embed(title='Tombola information for running tombola:', color=discord.Colour.dark_teal())
        embed.add_field(name='creator', value=f'{lockuser.display_name}', inline=True)
        embed.add_field(name='participant(s)', value=f'{participant_string}', inline=True)
        embed.set_footer(text="proudly presented by tombotla")
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Management(bot))